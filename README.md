# Simulation

## Table of Contents
- [Prerequisite](#Prerequisite)
- [Installation](#installation)
- [Usage](#usage)
- [Contributors](#contributors)
- [License](#license)

## Prerequisite

Tested with python 3.11

## Installation
Clone the repository:

git clone https://gitlab.com/Overlorde/simulation.git

## Usage

This project is aimed to optimize the cost of a call center such as number of employees depending on the number of calls and mails received over the day.

Execute with ```python python/main.py``` to find the optimized number of employee for the call center with input parameters.

#### Caution : Time of execution depends on both upper bound of research (number of employee) and CPU specifications.

## Contributors

@Overlorde
[...]

## License

- [BSD3](https://gitlab.com/Overlorde/simulation/LICENSE.txt)