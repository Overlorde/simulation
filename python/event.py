"""Event module"""
import logging

from python.stats_helper import StatsHelper

logging.basicConfig(encoding='utf-8', level=logging.DEBUG)
logger = logging.getLogger('Event')


class Event:
    """Represent an event in scheduler"""

    def __init__(self, scheduler, params):
        """
        Constructor of Event
        :param scheduler: the scheduler
        :param params: the parameters (n,nc,nt,ntMax)
        """
        self.qat = 0
        self.qct = [0] * StatsHelper.uniform(20, 80)
        self.n = params["n"]
        self.nc = params["nc"]
        self.nt = params["nt"]
        self.nt_max = params["ntMax"]
        self.bit = [0] * self.n
        self.t_call = 0
        self.t_mail = 0
        # Indicateurs Stats
        self.time_wait_mail = 0
        self.area_qa = 0
        self.area_qc = 0
        self.x = 0
        self.area_t = 0
        self.area_bi = 0
        self.scheduler = scheduler

    def start(self):
        """
        Starts the scheduler
        :return: None
        """
        logger.info('start')
        self.scheduler.add_event(240, self.end)
        self.scheduler.add_event(StatsHelper.exponential(5), self.stop_call)
        self.scheduler.add_event(StatsHelper.exponential(0.5), self.stop_mail)

    def end(self):
        """
        End the scheduler
        :return: None
        """
        logger.info("""
        Simulation parameters:
        n: %s
        nc: %s
        nt: %s
        nt_max: %s
        """,
                    self.n, self.nc,
                    self.nt, self.nt_max)
        logger.info(
            """
            Scenario ending stats:
            qat: %s
            qct: %s
            qct + nc: %s
            tapp: %s
            courr: %s
            aireQa: %s
            aireQc: %s
            aireT: %s
            aireBi: %s
            Waiting time mails: %.2f minutes
            Waiting time calls: %.2f minutes
            Number of mails processed: %s
            Number of calls processed: %s
            Average call queue size: %.1f
            Average mail queue size: %.1f
            Usage rate of teleconsultants: %.1f
            Usage rate of telephone sets: %.1f
            Average mail answering delay: %.1f
            """,
            self.qat,
            self.qct.__len__(),
            self.qct.__len__() + self.count_employee_status(1),
            self.t_call, self.t_mail,
            self.area_qa, self.area_qc,
            self.area_t, self.area_bi,
            self.time_wait_mail,
            self.area_qa / self.t_call,
            self.t_mail,
            self.t_call,
            self.area_qa / 240,
            self.area_qc / 240,
            (self.area_bi / 240) * 100 / self.n,
            ((self.area_t / 240) * 100) / self.nt_max,
            self.time_wait_mail / self.t_mail)
        # sleep(5)
        self.scheduler.clear()

    def stop_call(self):
        """
        Stops the call events
        :return: None
        """
        self.x += 1
        logger.info('stop call')
        if self.scheduler.current_time < 60:
            self.scheduler.add_event(StatsHelper.exponential(5), self.stop_call)
        elif self.scheduler.current_time < 180:
            self.scheduler.add_event(StatsHelper.exponential(1), self.stop_call)
        else:
            self.scheduler.add_event(StatsHelper.exponential(10), self.stop_call)
        self.qat = self.qat + 1
        if (self.count_employee_status(0) > 0) and (self.count_employee_status(2) < self.nt_max):
            self.scheduler.add_event(0, self.serve_call)

    def stop_mail(self):
        """
        Stops the mail events
        :return: None
        """
        logger.info('stop mail')
        if self.scheduler.current_time < 60:
            self.scheduler.add_event(StatsHelper.exponential(0.5), self.stop_mail)
        else:
            self.scheduler.add_event(StatsHelper.exponential(5), self.stop_mail)
        self.qct.append(self.scheduler.current_time)
        if self.count_employee_status(0) > 0:
            self.scheduler.add_event(0, self.serve_mail)

    def serve_call(self):
        """
        Attributes a call to a server
        :return: None
        """
        logger.info('serve call')
        logger.debug("count employee status: %s", self.count_employee_status(2))
        self.qat = self.qat - 1
        self.t_call = self.t_call + 1
        self.change_employee_status(0, 2)
        self.scheduler.add_event(StatsHelper.uniform(5, 15), self.end_call)

    def serve_mail(self):
        """
        Attributes a mail to a server
        :return: None
        """
        logger.info('serve mail')
        self.time_wait_mail += self.scheduler.current_time - self.qct.pop(0)
        self.t_mail = self.t_mail + 1
        self.change_employee_status(0, 1)
        self.scheduler.add_event(StatsHelper.uniform(3, 7), self.end_mail)

    def end_call(self):
        """
        Ends a call task
        :return: None
        """
        logger.info('end call')
        self.change_employee_status(2, 0)
        if self.qat > 0:
            self.scheduler.add_event(0, self.serve_call)
        elif self.qct.__len__() > 0:
            self.scheduler.add_event(0, self.serve_mail)

    def end_mail(self):
        """
        Ends a mail task
        :return: None
        """
        logger.info('end mail')
        self.change_employee_status(1, 0)
        if (self.qat > self.count_employee_status(2)) and \
                (self.count_employee_status(2) < self.nt_max):
            self.scheduler.add_event(0, self.serve_call)
        elif self.qct.__len__() > 0:
            self.scheduler.add_event(0, self.serve_mail)

    def count_employee_status(self, status: int):
        """
        Counts the number of server with corresponding status
        :param status: the status of the server (0,1,2)
        :return: the sum of the server with the corresponding status
        """
        return sum(x == status for x in self.bit)

    def change_employee_status(self, current_status: int, new_status: int):
        """
        Updates the employee status
        :param current_status: the current status of the employee
        :param new_status: the new status of the employee
        :return: None
        """
        self.bit[self.bit.index(current_status)] = new_status

    def update_areas(self, date_end: float, date: float):
        """
        Update the QA & QC area
        :param date_end: the end date date as float
        :param date: the start date as float
        :return: None
        """
        self.area_qa = self.area_qa + (date - date_end) * self.qat
        self.area_qc = self.area_qc + (date - date_end) * self.qct.__len__()
        nb_t1 = 0
        for var_x in self.bit:
            if var_x == 1:
                nb_t1 += 1

        nb_t2 = 0
        for var_x in self.bit:
            if var_x == 2:
                nb_t2 += 1
        self.area_bi = self.area_bi + (date - date_end) * (nb_t1 + nb_t2)
        self.area_t = self.area_t + (date - date_end) * nb_t2
