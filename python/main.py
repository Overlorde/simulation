"""Main module"""
import numpy as np

from python.scheduler import Scheduler
from python.event import Event, logger

np.seterr(divide='ignore')


def start():
    """Allow to start the call center simulation"""
    params = []
    for i in range(4, 20):
        for j in range(i, 0, -1):
            for k in range(2, i):
                if (i - j) <= k:
                    params.append({"n": i, "nc": j, "nt": i - j, "ntMax": k, "loops": 100})

    for param in params:
        loops = param["loops"]
        sum_mail_nt = 0  # nombre de courriels non traités
        sum_wait_time_avg = 0  # temps attente téléphone
        sum_tu_server = 0  # taux utilisation téléconseillers
        sum_tu_position = 0  # taux utilisations postes téléphonique
        sum_tps_mail = 0  # temps traitement courriels

        for _ in range(loops):
            scheduler = Scheduler()
            event = Event(scheduler, param)
            scheduler.add_event(0, event.start)
            while not scheduler.is_empty():
                next_event = scheduler.take_next_event()
                start_event = scheduler.current_time
                end_event = next_event['date']
                event.update_areas(start_event, end_event)
                scheduler.current_time = next_event['date']
                next_event['event']()
            sum_mail_nt += event.qct.__len__() + event.count_employee_status(1)

            sum_wait_time_avg += np.divide(event.area_qa, event.t_call)
            sum_tu_server += np.divide((event.area_bi / 240) * 100, event.n)
            sum_tu_position += np.divide(((event.area_t / 240) * 100), event.nt_max)
            sum_tps_mail += np.divide(event.time_wait_mail, event.t_mail)

        logger.info("""Stats:
                        n:%s
                        nc:%s
                        nt:%s
                        ntMax:%s
                        average mail not processed: %s
                        average waiting time: %s minutes
                        average tu server: %s
                        average tu position: %s
                        average tps mail:%s
                        """,
                    param['n'],
                    param['nc'],
                    param['nt'],
                    param['ntMax'],
                    sum_mail_nt / loops,
                    sum_wait_time_avg / loops,
                    sum_tu_server / loops,
                    sum_tu_position / loops,
                    sum_tps_mail / loops)


if __name__ == '__main__':
    start()
