"""Scheduler module"""


class Scheduler:
    """Handle the event scheduler"""

    def __init__(self):
        """
        Constructor by default of Scheduler
        """
        self.scheduler = {}
        self.current_time = 0

    def take_next_event(self):
        """
        Returns the next event in scheduler
        :return: the next event in scheduler
        """
        key = min(self.scheduler.keys())
        dict_event = dict()
        event = self.scheduler[key][0]
        del self.scheduler[key][0]
        if not self.scheduler[key]:
            del self.scheduler[key]
        dict_event['date'] = key
        dict_event['event'] = event
        return dict_event

    def add_event(self, delta: float, event: ()):
        """
        Add an event to the scheduler
        :param delta: the delta in time
        :param event: the event
        :return: None
        """
        if self.current_time + delta in self.scheduler:
            self.scheduler[self.current_time + delta].append(event)
        else:
            self.scheduler[self.current_time + delta] = [event]

    def clear(self):
        """
        Clear the scheduler
        :return: None
        """
        self.scheduler.clear()

    def is_empty(self):
        """
        Checks if scheduler is empty
        :return: a boolean
        """
        return len(self.scheduler) == 0
