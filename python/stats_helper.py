"""Stats helper module"""
import numpy as np


class StatsHelper:
    """Handle the generated number of the simulation events"""

    @staticmethod
    def uniform(a, b):
        """
        Creates a random integer numbers from a to b included with a uniform law
        :param a: the lower bound
        :param b: the upper bound
        :return: the generated number
        """
        return int(np.random.uniform(a, b))

    @staticmethod
    def exponential(a):
        """
        Creates a random number starting from a lower bound included with an exponential law
        :param a: the lower bound
        :return: the generated number
        """
        return np.random.exponential(a)
